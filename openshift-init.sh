oc new-project assistant
oc create secret generic assistantkey --from-file=ssh-privatekey=/Users/bertrandlarsy/.ssh/assistant --type=kubernetes.io/ssh-auth
oc secrets link builder assistantkey
oc new-app git@bitbucket.org:blarsy/assistant-api.git --source-secret=assistantkey
oc patch service assistant-api -p '{"spec":{"ports":[{"name": "4000-tcp", "port": 4000, "protocol": "TCP", "targetPort": 4000}]}}'
oc expose service assistant-api --name=socket --port=8080
oc expose svc/assistant-api --name=api --path=/graphql --port=4000
oc logs -f bc/assistant-api
