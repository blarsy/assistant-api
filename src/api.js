import express from 'express'
import graphqlHTTP from 'express-graphql'
import { buildSchema } from 'graphql'
import agent from './agent'
import log from './log'

// Construct a schema, using GraphQL schema language
const schema = buildSchema(`
  type BuyInfo {
    currency: String!
    btcAmount: Float!
  }

  type Query {
    info: String!
  }

  type Mutation {
    reload: String!
    buyImmediately(buyInfo: BuyInfo): String!
    sellImmediately(currency: String): String!
  }
`)
const ensureAuthorized = (req, res, next) => {
  if (req.method !== 'POST') {
    next()
    return
  }
  if (
    !req.header('authorization') ||
    req.header('authorization') !==
      'Basic LcEWrpDVnniDb4Pw2riUdYiY696jpa7w6ahakRGndeUDZpN3vQir9XCU9ryBS3bQiWazzhujDWE7ua2NzPUBdFFbLd8bhVpCSz9Tnst5MacXfUyVWPQBxTx9'
  ) {
    log.error(`Whoops, that one did not pass authentication: ${req}`)
    next(new Error('Unauthorized'))
  }

  next()
}

// The root provides a resolver function for each API endpoint
const root = {
  info: () => {
    return 'Up'
  },
  reload: async () => {
    try {
      await agent.startOrRestartBroadcasting()
      return 'Ok'
    } catch (ex) {
      log.error(ex)
      return ex.toString()
    }
  },
  buyImmediately: async buyInfo => {
    try {
      await agent.buyImmediately(buyInfo.currency, buyInfo.btcAmount)
      return 'Ok'
    } catch (ex) {
      log.error(ex)
      return ex.toString()
    }
  },
  sellImmediately: async currency => {
    try {
      await agent.sellImmediately(currency)
      return 'Ok'
    } catch (ex) {
      log.error(ex)
      return ex.toString()
    }
  }
}

const app = express()
app.use(ensureAuthorized)
app.use(
  '/graphql',
  graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true
  })
)
app.listen(4000)
log.info('Running a GraphQL API server at localhost:4000/graphql')
