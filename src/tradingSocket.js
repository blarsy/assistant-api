import ws from 'ws'
import EventEmitter from 'events'

class TradingSocket extends EventEmitter {
  constructor() {
    super()
    this.socketServer = new ws.Server({ port: 8080 })

    this.socketServer.on('connection', ws => {
      this.emit('ready')
    })

    this.send = this.send.bind(this)
  }

  send(data) {
    this.socketServer.clients.forEach(client => {
      try {
        client.send(JSON.stringify(data))
      } catch (ex) {
        log.error(ex)
      }
    })
  }
}

export default new TradingSocket()
