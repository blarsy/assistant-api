import winston from 'winston'

export default winston.createLogger({
  transports: [
    new winston.transports.Console({ level: 'info' }),
    new winston.transports.File({
      filename: './log.txt',
      level: 'verbose',
      maxsize: 5000000,
      maxFiles: 3,
      tailable: true
    })
  ]
})
