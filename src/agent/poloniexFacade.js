import { get } from 'axios'
import { createWriteStream } from 'fs'
import { contains, forEach } from 'ramda'
import Poloniex from 'poloniex-api-node'
import EventEmitter from 'events'
import util from 'util'
import settings from './settings.json'
import log from '../log'

class PoloniexFacade extends EventEmitter {
  constructor(watchedPairs, appId, secret) {
    super()
    this.watchedPairs = watchedPairs
    this.initializing = false
    this.poloniex = new Poloniex(appId, secret)
    this.logStream = null
  }

  startToEmitOrderbooksUpdates() {
    forEach(pair => this.poloniex.subscribe(pair), this.watchedPairs)

    this.poloniex.on('open', () => {
      log.info('Poloniex WebSocket connection open')
    })

    this.poloniex.on('close', () => {
      log.info('Poloniex WebSocket connection disconnected')
    })

    this.poloniex.on('error', error => {
      log.error(`An error has occured: ${JSON.stringify(error)}`)
    })

    this.poloniex.on('message', this.messageReceived.bind(this))
    this.poloniex.openWebSocket()
  }

  logAndReturn(methodName, res) {
    try {
      log.verbose(`Method ${methodName}:`, res)
    } catch (ex) {
      log.error(`Error while logging call to poloniex api ${methodName}: ${ex}`)
    }
    return res
  }

  async messageReceived(channelName, data, seq) {
    try {
      if (contains(channelName, this.watchedPairs)) {
        this.emit('orderBookModify', channelName, data, seq)
      }
    } finally {
      log.verbose('Websocket message received', {
        channelName,
        seq,
        data
      })
    }
  }

  async returnCompleteBalances() {
    return this.logAndReturn(
      'returnCompleteBalances',
      await this.poloniex.returnCompleteBalances()
    )
  }
  async returnOpenOrders() {
    return this.logAndReturn(
      'returnOpenOrders',
      await this.poloniex.returnOpenOrders('all')
    )
  }
  async returnMyTradeHistory(...args) {
    return this.logAndReturn(
      'returnMyTradeHistory',
      await this.poloniex.returnMyTradeHistory(...args)
    )
  }

  async callMethod(method) {
    const res = await get(method)
    return this.logAndReturn(method, res)
  }

  async returnTicker() {
    return this.logAndReturn('returnTicker', await this.poloniex.returnTicker())
  }

  async returnChartData(...args) {
    return this.logAndReturn(
      'returnChartData',
      await this.poloniex.returnChartData(...args)
    )
  }

  async sell(...args) {
    return this.logAndReturn('sell', await this.poloniex.sell(...args))
  }

  async buy(...args) {
    return this.logAndReturn('buy', await this.poloniex.buy(...args))
  }

  async cancelOrder(...args) {
    return this.logAndReturn(
      'cancelOrder',
      await this.poloniex.cancelOrder(...args)
    )
  }
}

export default new PoloniexFacade(
  settings.watchedPairs,
  settings.poloniex.appId,
  settings.poloniex.secret
)
