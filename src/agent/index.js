import EventEmitter from 'events'
import { find, map, reduce, takeWhile } from 'ramda'
import { getAlerts } from './alerter'
import { calculateBestPricesFromOrderbook, getLowsAndHighs } from './calculator'
import settings from './settings.json'
import poloniex from './poloniexFacade'
import queuer from './queuer'
import log from '../log'

const updateOrderBook = (channelName, data, currentOrderbooks) => {
  const orderbooks = currentOrderbooks
  for (let i = 0; i < data.length; i += 1) {
    const change = data[i]
    switch (change.type) {
      case 'orderBook':
        orderbooks[channelName] = change.data
        break
      case 'orderBookModify':
        if (change.data.type === 'ask') {
          orderbooks[channelName].asks[change.data.rate] = change.data.amount
        } else {
          orderbooks[channelName].bids[change.data.rate] = change.data.amount
        }
        break
      case 'orderBookRemove':
        if (change.data.type === 'ask') {
          delete orderbooks[channelName].asks[change.data.rate]
        } else {
          delete orderbooks[channelName].bids[change.data.rate]
        }
        break
      default:
    }
  }
}

const cancelOrders = async currency => {
  if (currency.openOrders && currency.openOrders.length > 0) {
    const promises = []
    for (let i = 0; i < currency.openOrders.length; i += 1) {
      promises.push(poloniex.cancelOrder(currency.openOrders[i]))
    }
    await Promise.all(promises)
  }
}

const getLastTrades = async () => {
  // Work only with trades from 90 days ago
  const numberOfSecsIn90Days = 60 * 60 * 24 * 90
  const trades = await poloniex.returnMyTradeHistory(
    'all',
    Math.round(Number(new Date() / 1000) - numberOfSecsIn90Days),
    Math.round(Number(new Date()) / 1000)
  )
  const acc = (current, trade) => {
    const target = current
    target.total += Number(trade.total)
    target.amount += Number(trade.amount)
    return target
  }

  const pairs = Object.keys(trades)

  return map(pair => {
    const pairTrades = trades[pair]
    if (pairTrades.length > 0) {
      const accumulator = { total: 0, amount: 0 }
      const history = { pair }
      let totals
      if (pairTrades[0].type === 'buy') {
        history.type = 'buy'
        const lastBuys = takeWhile(trade => trade.type === 'buy', pairTrades)
        totals = reduce(acc, accumulator, lastBuys)
      } else {
        history.type = 'sell'
        const lastSales = takeWhile(trade => trade.type === 'sell', pairTrades)
        totals = reduce(acc, accumulator, lastSales)
      }

      history.lastPrice = totals.total / totals.amount
      history.lastQuantity = totals.total
      return history
    }
    return {
      lastQuantity: 0
    }
  }, pairs)
}

const loadBalanceAndHistory = async currentData => {
  const data = currentData
  const balances = await poloniex.returnCompleteBalances()
  const history = await getLastTrades()
  const prices = await poloniex.returnTicker()
  const openOrders = await poloniex.returnOpenOrders()
  let totalValue = 0

  for (let i = 0; i < settings.watchedPairs.length; i += 1) {
    const pair = settings.watchedPairs[i]
    const currency = pair.substring(pair.indexOf('_') + 1)
    const lastHistory = find(historyItem => historyItem.pair === pair, history)
    const balance = balances[currency]
    const current = { currency, price: +prices[pair].last, pair }
    if (balance.btcValue > 0.0001) {
      current.btcAmount = +balance.btcValue
      totalValue += current.bestSell
        ? current.bestSell.btcAmount
        : current.btcAmount
      current.amount = +balance.available + +balance.onOrders
      if (
        lastHistory &&
        (lastHistory.type === 'buy' ||
          (currency === 'USDT' && lastHistory.type === 'sell'))
      ) {
        current.acquiredPrice = lastHistory.lastPrice
      }
    }
    if (
      lastHistory &&
      (lastHistory.type === 'sell' ||
        (currency === 'USDT' && lastHistory.type === 'buy'))
    ) {
      current.lastSalePrice = lastHistory.lastPrice
    }
    if (openOrders[pair] && openOrders[pair].length > 0) {
      current.openOrders = map(
        openOrder => openOrder.orderNumber,
        openOrders[pair]
      )
    }
    data.pairs[pair] = current
  }

  data.totalValue = totalValue.toFixed(8)
}

const fetchPastPrice = currentData => {
  log.info('Refreshing past prices ...')
  try {
    const data = currentData
    for (let i = 0; i < settings.watchedPairs.length; i += 1) {
      const pair = settings.watchedPairs[i]
      const chartPeriod = 300
      const pastDate = Math.abs((Number(new Date()) - 1800000) / 1000)

      queuer.execute(
        () =>
          new Promise(resolve => {
            // Fetch prices of 30 minutes ago
            poloniex
              .returnChartData(pair, chartPeriod, pastDate)
              .then(priceHistory => {
                const pastData = priceHistory[0]
                if (pastData.open !== 0) {
                  data.pairs[pair].pastPrice = {
                    time: pastDate,
                    price: pastData.open
                  }
                  resolve()
                }
              })
          })
      )
    }
  } catch (ex) {
    log.error(ex)
  }
}

const fetchPriceHistory = async currentData => {
  log.info(
    'Refreshing info about historic highs and lows for watched currencies...'
  )
  try {
    const data = currentData
    const priceHistoryDatas = await getLowsAndHighs(settings.watchedPairs)

    for (let i = 0; i < settings.watchedPairs.length; i += 1) {
      const pair = data.pairs[settings.watchedPairs[i]]
      const pairPrices = find(
        priceHistoryData => priceHistoryData.pair === settings.watchedPairs[i],
        priceHistoryDatas
      )
      if (pairPrices && pair) {
        pair.priceHistory = pairPrices
      }
    }
  } catch (ex) {
    log.error(ex)
  }
}

class Agent extends EventEmitter {
  constructor() {
    super()
    this.intervals = []
    this.data = { pairs: {} }

    this.orderbooks = {}
    poloniex.startToEmitOrderbooksUpdates()
    poloniex.on('orderBookModify', (channelName, data) => {
      updateOrderBook(channelName, data, this.orderbooks)
      calculateBestPricesFromOrderbook(
        { pair: channelName, orderbook: this.orderbooks[channelName] },
        this.data
      )
    })
    this.startOrRestartBroadcasting()
  }

  async startOrRestartBroadcasting() {
    log.info('Beginning or resetting regular crypto holding infos broadcasts.')
    const broadcast = () => {
      try {
        this.data.alerts = getAlerts(this.data)
        this.emit('loaded', this.data)
      } catch (ex) {
        log.error(ex)
      }
    }

    if (this.intervals.length === 2) {
      clearInterval(this.intervals[0])
      clearInterval(this.intervals[1])
      this.intervals.pop()
      this.intervals.pop()
    }
    try {
      await loadBalanceAndHistory(this.data)
    } catch (ex) {
      log.error(ex)
    }
    broadcast()
    this.intervals.push(setInterval(() => broadcast(), 5000))

    fetchPastPrice(this.data)
    this.intervals.push(setInterval(() => fetchPastPrice(this.data), 60000))

    fetchPriceHistory(this.data)
    this.intervals.push(
      setInterval(() => fetchPriceHistory(this.data), 3600000)
    )
  }

  async sellImmediately(currency) {
    if (typeof currency !== 'object') {
      currency = find(pair => pair.currency === currency, data.pairs)
      if (!currency) {
        throw new Error('Currency not found.')
      }
    }
    await cancelOrders(currency)
    await poloniex.sell(
      currency.pair,
      currency.bestSell.realizedPrice,
      currency.amount
    )
    this.startOrRestartBroadcasting()
  }

  async buyImmediately(currency, btcAmount) {
    if (typeof currency !== 'object') {
      currency = find(pair => pair.currency === currency, data.pairs)
      if (!currency) {
        throw new Error('Currency not found.')
      }
    }
    if (currency.bestBuy) {
      const targetBuy = find(
        bestBuy => bestBuy.btcAmount === btcAmount,
        currency.bestBuy
      )
      if (targetBuy) {
        await cancelOrders(currency)
        await poloniex.buy(
          currency.pair,
          targetBuy.realizedPrice,
          targetBuy.amount
        )
        this.startOrRestartBroadcasting()
      } else {
        throw new Error(
          `No buy info found for ${btcAmount}BTC on ${currency.currency}`
        )
      }
    } else {
      throw new Error(
        `No buy info found for ${btcAmount}BTC on ${currency.currency}`
      )
    }
  }
}

export default new Agent()
