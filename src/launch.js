// startup agent
import agent from './agent/'
import tradingSocket from './tradingSocket'
import './api'
import log from './log'

log.info(`launching app at ${new Date()}`)

// Launch web socket
tradingSocket.once('ready', () => {
  agent.on('loaded', data => {
    log.info('Loaded event raised, sending to all socket clients')
    tradingSocket.send(data)
  })
})
